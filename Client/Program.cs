﻿// Importations & Librairies
using System;
using System.Net;
using System.Text;
using System.Net.Sockets ;

// --------------------------- //
// --- NetcatLike - CLIENT --- //
// --------------------------- //

namespace ReseauB2_Client {

    class Program {
        static void Main(string[] args) {
            // Paramètres & Configuration
            IPAddress ip = IPAddress.Parse("127.0.0.1");
            int port = 5000;

            // Création du client
            TcpClient client = new TcpClient();
            client.Connect(ip, port);
            Console.WriteLine("[SYSTEME] ------------------------------");
            Console.WriteLine("[SYSTEME] Vous êtes connecté au serveur");
            Console.WriteLine("[SYSTEME] Pour vous déconnecter : envoyez un message vide");
            Console.WriteLine("[SYSTEME] Good tchating time !");
            Console.WriteLine("[SYSTEME] ------------------------------");
            NetworkStream ns = client.GetStream();
            Thread thread = new Thread(o => ReceiveMessages((TcpClient)o));

            thread.Start(client);

            string s;
            while (!string.IsNullOrEmpty((s = Console.ReadLine()))) {
                byte[] buffer = Encoding.ASCII.GetBytes(s);
                ns.Write(buffer, 0, buffer.Length);
            }

            client.Client.Shutdown(SocketShutdown.Send);
            thread.Join();
            ns.Close();
            client.Close();
            Console.WriteLine("[SYSTEME] ------------------------------");
            Console.WriteLine("[SYSTEME] Vous avez été déconnecté du serveur");
            Console.WriteLine("[SYSTEME] A très bientôt !");
            Console.WriteLine("[SYSTEME] ------------------------------");
            Console.ReadKey();
        }

        // Réception des messages
        static void ReceiveMessages(TcpClient client) {
            NetworkStream ns = client.GetStream();
            byte[] receivedBytes = new byte[1024];
            int byte_count;

            while ((byte_count = ns.Read(receivedBytes, 0, receivedBytes.Length)) > 0) {
                Console.Write("[MESSAGE] "+ Encoding.ASCII.GetString(receivedBytes, 0, byte_count));
            }
        }
    }

}
