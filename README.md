# Réseau B2 - Projet Final

## Description

Notre projet final de réseau est une application en C# permettant de communiquer sur un tchat.

Un serveur est lancé, et plusieurs clients peuvent s'y connecter afin d'échanger des messages.

## Utilisation serveur

Tout d'abord, il faut configurer le serveur en allant dans le fichier `Program.cs` et en modifiant la variable `port`.

Afin d'utiliser le "NetcatLike", il faut clone le repository, vous rendre dans le dossier "Serveur" afin de le lancer avec la commande `dotnet run`.

## Utilisation clients

Tout d'abord, il faut configurer les clients afin qu'ils se connectent au bon serveur en allant dans le fichier `Program.cs` et en modifiant les deux variables `ip` et `port`.

Pour lancer des clients, il faut aller dans le dossier "Client" et le lancer avec `dotnet run`.

Il faut connecter plusieurs clients pour pouvoir discuter, et le serveur reçoit des logs de toutes les conversations.

Pour quitter la session de discussion, il suffit d'envoyer un message vide.