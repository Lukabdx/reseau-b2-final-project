﻿// Importations & Librairies
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

// ---------------------------- //
// --- NetcatLike - SERVEUR --- //
// ---------------------------- //

namespace ReseauB2_Serveur {

    class Program {
        static readonly object _lock = new object();
        static readonly Dictionary<int, TcpClient> liste_clients = new Dictionary<int, TcpClient>();

        static void Main(string[] args) {
            // Paramètres & Configuration
            int port = 5000;

            // Autres
            int count = 1;

            // Création du serveur
            TcpListener ServerSocket = new TcpListener(IPAddress.Any, port);
            ServerSocket.Start();

            while (true) {
                TcpClient client = ServerSocket.AcceptTcpClient();
                lock (_lock) liste_clients.Add(count, client);
                Console.WriteLine("[SYSTEME] Un utilisateur s'est connecté.");

                Thread t = new Thread(handle_clients);
                t.Start(count);
                count++;
            }
        }


        public static void handle_clients(object o) {
            int id = (int)o;
            TcpClient client;

            lock (_lock) client = liste_clients[id];

            while (true) {
                NetworkStream stream = client.GetStream();
                byte[] buffer = new byte[1024];
                int byte_count = stream.Read(buffer, 0, buffer.Length);

                // Si l'utilisateur n'a encore rien envoyé
                if (byte_count == 0) {
                    break;
                }

                // Si l'utilisateur a envoyé quelque chose
                string data = Encoding.ASCII.GetString(buffer, 0, byte_count);
                SendMessages(data);
                Console.WriteLine("[MESSAGE] "+data);
            }

            lock (_lock) liste_clients.Remove(id);
            client.Client.Shutdown(SocketShutdown.Both);
            client.Close();
        }

        // Envoi des messages à tous les clients connectés au serveur
        public static void SendMessages(string data) {
            byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

            lock (_lock) {
                foreach (TcpClient client in liste_clients.Values) {
                    NetworkStream stream = client.GetStream();

                    stream.Write(buffer, 0, buffer.Length);
                }
            }
        }
    }

}
